# Groovy Script Example

Example script that uses the crossplatform jSerialComm Java library to communicate over the serial port.

The script expects an ODS USB-150 sensor in ASCII mode.

## Requirements

 * Java JDK 7 or 8 - Oracle or OpenJDK.
 * [Groovy 2.5.x](http://groovy-lang.org/download.html) install w. [sdkman](https://sdkman.io/) (for Mac and Linux) or the [Windows MSI](https://dl.bintray.com/groovy/Distributions/groovy-2.5.7-installer.exe) installer.


## Usage

    $ groovy Serial.groovy -h
    usage: groovy Serial [options]
     -b,--baud <arg>   baudrate (default: 38400)
     -h,--help         usage information
     -l,--list         list available ports
     -p,--port <arg>   serial port (default: /dev/ttyUSB0)
