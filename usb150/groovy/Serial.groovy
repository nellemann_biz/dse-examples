@Grab(group='com.fazecast', module='jSerialComm', version='[2.0.0,3.0.0)')

/*
  Examples on https://github.com/Fazecast/jSerialComm/wiki/Usage-Examples
*/

import com.fazecast.jSerialComm.SerialPort
import com.fazecast.jSerialComm.SerialPortEvent
import com.fazecast.jSerialComm.SerialPortDataListener


/*
  Default values
*/

String portname = "/dev/ttyUSB0"
String baudrate = "38400"


/*
  Parse command line arguments
*/

CliBuilder cli = new CliBuilder(usage: 'groovy Serial [options]')
cli.with {
  p longOpt: 'port', args: 1, required: false, 'serial port (default: /dev/ttyUSB0)'
  b longOpt: 'baud', args: 1, required: false, 'baudrate (default: 38400)'
  l longOpt: 'list', args: 0, required: false, 'list available ports'
  h longOpt: 'help', args: 0, required: false, 'usage information'
}

OptionAccessor options = cli.parse(args)
if (!options) {
  return
}

if(options.h) {
  cli.usage()
  return
}

if (options.p) {
  portname = options.p
}

if (options.b) {
  baudrate = options.b
}

if (options.l) {
  SerialPort.getCommPorts().each() { SerialPort port ->
    println("> " + port.getSystemPortName() + " - " + port.getDescriptivePortName())
  }
  System.exit(0)
}



/*
  Open serial port
*/

SerialPort comPort = SerialPort.getCommPort(portname)
comPort.setBaudRate(baudrate as Integer)
comPort.openPort();


/*
  Setup data listener to avoid polling port for data
*/

comPort.addDataListener(new SerialPortDataListener() {

  @Override
  public int getListeningEvents() { return SerialPort.LISTENING_EVENT_DATA_RECEIVED; }

  @Override
  public void serialEvent(SerialPortEvent event)
  {
    byte[] newData = event.getReceivedData();
    print(newData.length)
    for (int i = 0; i < newData.length; ++i) {
      print((char)newData[i])
    }
    println()
    sleep(400)
  }
});


/*
  Read from standard input and send to serial port
*/

String input
while(true) {
  input =  System.console().readLine()
  comPort.writeBytes(input.getBytes(), input.length())
}

comPort.removeDataListener();
comPort.closePort()
